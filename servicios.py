#! /usr/bin/env python
# -*- coding: utf-8 -*-
import socket, subprocess
import requests
import time
import urllib.request #Agregando web scraping
import sys
from bs4 import BeautifulSoup
from bs4 import Comment




def probarServicios(ip,port,path,sistema):
	mensaje=""
	statusCode=""
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #HAHH: Crear socket
	ruta=""
	datos=""
	try:

		#HAHH: Construir ruta en funcion de los sistemas para identificar los códigos http de respuesta
		if sistema == "VOL-Producción":
			ruta="http://"+str(ip)+":"+str(port)+path
			#HAHH: Extraer la marca de agua mediante web scraping
			datos = urllib.request.urlopen(ruta).read().decode('ISO-8859-1')
			soup =  BeautifulSoup(datos,'html.parser')
			comentarios = soup.find_all(text=lambda text: isinstance(text, Comment))
			marcaComentario = str(comentarios[5])#HAHH: En vol el 5to comentario es la marca de agua que deseamos mostrar
			marca_agua = BeautifulSoup(marcaComentario,'html.parser').contents[0]
		if sistema == "VOB-Producción":
			ruta="http://"+str(ip)+":"+str(port)+path
			#HAHH: Extraer la marca de agua mediante web scraping
			datos = urllib.request.urlopen(ruta).read().decode('ISO-8859-1')
			soup =  BeautifulSoup(datos,'html.parser')
			comentarios = soup.find_all(text=lambda text: isinstance(text, Comment))
			marcaComentario = str(comentarios[16])#HAHH: En vob el 16vo comentario es la marca de agua que deseamos mostrar
			marca_agua = BeautifulSoup(marcaComentario,'html.parser').contents[0]
			#marca_agua = "Sin marca de agua"
		if sistema == "VOB-local":
			ruta="http://"+str(ip)+":"+str(port)+path
			#HAHH: Extraer la marca de agua mediante web scraping
			datos = urllib.request.urlopen(ruta).read().decode('ISO-8859-1')
			soup =  BeautifulSoup(datos,'html.parser')
			comentarios = soup.find_all(text=lambda text: isinstance(text, Comment))
			marcaComentario = str(comentarios[16])#HAHH: En vob el 16vo comentario es la marca de agua que deseamos mostrar
			marca_agua = BeautifulSoup(marcaComentario,'html.parser').contents[0]

        #HAHH: Evitar redireccionamiento
		r = requests.get(ruta,allow_redirects=False)
		#print(r.history)
		
		statusCode = str(r.status_code)
		
		#HAHH: Iniciar comunicacion socket con el servidor para identificar status del servidor y realizar otras acciones
		s.connect((ip, int(port)))
		s.shutdown(2)
		mensaje="  |       UP      | "+ip+" |  "+port+"  | "+sistema+" |       "+statusCode+"       |       "+marca_agua
		
	except: 
		#subprocess.call(['/etc/init.d/apache2', 'start'])#HAHH: Ejemplo se si quisiera encender el apache con subprocess
		mensaje="  |      DOWN     | "+ip+" |  "+port+"  | "+sistema+" |       No Aplica      |       No Aplica"
		
        
		#raise requests.HTTPError(e.response.text, response=e.response)
	s.close() #cerrar socket
	return mensaje

	

#HAHH: Almacenar los servidores en una matriz [ip,puerto,path,descripcion del sistema que lo usa como referencia]
hosts= [
#Segmento de red antiguo VOL
#['180.180.169.13','8180','/VOL-Web/login.action','VOL-Producción'],
#['180.180.169.15','8180','/VOL-Web/login.action','VOL-Producción'],
#['180.180.169.112','8080','/VOL-Web/login.action','VOL-Producción'],

#['180.180.169.115','8080','/VOL-Web/login.action','VOL-Producción'],
#Nuevo segmento de red de VOL
['172.27.31.112','8080','/VOL-Web/login.action','VOL-Producción'],
['172.27.31.13','8180','/VOL-Web/login.action','VOL-Producción'],
['172.27.31.82','8080','/VOL-Web/login.action','VOL-Producción'],
['172.27.31.15','8180','/VOL-Web/login.action','VOL-Producción'],
['172.27.31.115','8080','/VOL-Web/login.action','VOL-Producción'],
#['180.180.169.21','8180','/VOL-Web/login.action','VOL-Desarrollo'],
#['180.180.169.22','8180','/VOL-Web/login.action','VOL-Controlada'],
#['180.180.169.18','8080','/VOL-Web/login.action','VOL-Auditoria']
#['172.30.2.158','8080','/VOL-Web/login.action','VOL-Localhost'],
#Segmento de red antiguo VOB
#['180.180.169.114','8180','/VOB/','VOB-Producción'],
#['180.180.169.111','8180','/VOB/','VOB-Producción'],
#['180.180.169.12','8180','/VOB/','VOB-Producción'],
#['180.180.169.14','8180','/VOB/','VOB-Producción'],
#Nuevo segmento de red de VOB
['172.27.31.114','8180','/VOB/jsp/index.jsp','VOB-Producción'],
['172.27.31.111','8180','/VOB/jsp/index.jsp','VOB-Producción'],
['172.27.31.81','8180','/VOB/jsp/index.jsp','VOB-Producción'],
['172.27.31.12','8180','/VOB/jsp/index.jsp','VOB-Producción'],
['172.27.31.14','8180','/VOB/jsp/index.jsp','VOB-Producción']
#,['172.30.2.122','8080','/VOB/','VOB-local']
]

#HAHH: Imprimir la hora de consulta con el modulo time
#print (time.strftime("%d/%m/%y"))
print ("Fecha y hora " + time.strftime("%c"))


#HAHH: Recorrer la matriz con los servicios
print ("  | STATUS SERVER |       IP        | PUERTO |     SISTEMA    | STATUS REQUEST |        MARCA DE AGUA ")
for i in range(len(hosts)):
	if hosts[i][3] =="VOL-Producción":
		print (probarServicios(hosts[i][0],hosts[i][1],hosts[i][2],hosts[i][3]))
	if hosts[i][3] =="VOB-Producción":
		print (probarServicios(hosts[i][0],hosts[i][1],hosts[i][2],hosts[i][3]))
	if hosts[i][3] =="VOB-local":
		print (probarServicios(hosts[i][0],hosts[i][1],hosts[i][2],hosts[i][3]))

    