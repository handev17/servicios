Scripts desarrollado en Python 3 que valida el status de servidor(es) (Encendido o apagado) mediante socket e identifica los códigos de status HTTP mediante la inclusion de la libreria request evitando el redireccionamiento.
 
Se requiere la instalación de las siguientes dependencias:

pip install socket.py

pip install subprocess.run

pip install requests

pip install beautifulsoup4
